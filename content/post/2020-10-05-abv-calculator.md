---
title: ABV Calculator
date: 2020-10-05
---

I have just posted the code for my new ABV Calculator. This is a calculator specifically for those that use refractometers when calculating ABV for their beer or wine.

I designed it to run on Linux desktops and Linux Phones/Tablets

**Warning:** This calculator may not be super precise. Please use this calculator at your own risk, and verify your ABV with other sources before making any decisions. I take no responsibility for any decisions made from using this app.

Source Code: <br>
https://gitlab.com/The3DmaN/abv-calc