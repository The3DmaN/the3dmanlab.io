---
title: Idok Remote (for Kodi)
date: 2021-03-09
---

For all you Kodi fans out there, I just started working on a new Kodi remote for Linux Mobile (also works on desktop). This remote features the ability to connect to multiple instances and has a combobox selection to choose which one to control right on the remote screen. Perfect for those that have multiple RPI's with Kodi.  For right now the other features are pretty basic and limited to button controls only, but I plan to add many more features in the future, so stay tuned.

# Install:

Manjaro/Arch:


1) Install from AUR `yay idokremote-git`


or


2) Download PKGBUILD from: https://gitlab.com/The3DmaN/idokremote


cd to directory you downloaded it to.  Ex: `cd ~/Downloads`


Run `makepkg -si`

# Screenshot

![Screenshot](/idokremote.jpg "Remote Screenshot")
