---
title: Window Borders on PinePhone QtQuick Apps
date: 2021-08-08
---

Recently PureOS and Arch were updated on the PinePhone to turn on window borders for QtQuick apps so that in convergence mode the apps can be resized and moved around. (See here: https://github.com/dreemurrs-embedded/Pine64-Arch/issues/197)

While these changes are nice for convergence mode. In my opinion the average non-poweruser or non-tinkerer is likely not using convergence mode too often and now the users who use the PinePhone in mobile mode have to deal with window borders that are distracting from the app content and that shrink the apps usable space on an already tiny screen.

Don't get me wrong I understand the change and it is kind of a no win situation. I just don't agree with changing it to please the small number of convergence users. There has to be a better solution to please both types of users.

For the time being and to help out the users of my apps, I have added in new code to detect if the screen is less than 800px and if it is remove the window borders. This is a quick and dirty fix that resolves the problem for both mobile and convergence users, however I am not sure ultimately this is the correct way to implement this due to many different devices and screen sizes. My hope is that in the future Mobile OS devs can find a way to implement this on the OS side upon change to convergence mode to eliminate QtQuick app devs from having to change based on screen size.

If anyone has any better ideas on how to implement this better in QML please reach out to me on Matrix (I am usually in the PinePhone and Arch Linux on Mobile channels)

QML Added to Main in all my QtQuick Apps:
```
flags: {
        if (screen.width < 800) {
            Qt.FramelessWindowHint
        }
    }
```



