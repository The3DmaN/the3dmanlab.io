---
title: ABV Calculator Update
date: 2020-11-12
---

I have re-developed and updated the code for my ABV Calculator. I moved over to Gtk, Glade and Python for development. I moved away from QT due to all the dependencies required for such a simple app, as well as all the extra difficulties with packaging. Going forward I will only be developing/supporting this app for Gtk/Gnome based Linux desktop and Linux ARM64(AArch64) phones (PinePhone) and tablets.

While I am not new to basic app development, I am completely self taught. This means you may find errors and even some poor coding practices in my source. I have also never publically distributed any of the previous apps I made, since most of them were to do basic things for myself. Up until now I have not even used git much. I am still learning the ropes when it comes properly packaging apps for distribution. For now I have provided AUR and PKGBUILD options for Arch/Manjaro users. Users on other distros should also be able to run it by downloading the source below then cd into the abvcalc directory and run `python3 __init__.py`


**Warning:** This calculator may not be super precise. Please use this calculator at your own risk, and verify your ABV with other sources before making any decisions. I take no responsibility for any decisions made from using this app.

The Source Code can be found here: <br>
https://gitlab.com/The3DmaN/abv-calc

On Arch and Manjaro ABV Calculator can be installed from AUR `yay abv-calc-git` or by downloading the PKGBUILD in the above source and running `makepkg -si`
