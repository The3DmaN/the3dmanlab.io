---
title: Added aarch64 deb files for Apps
date: 2022-02-16
---

I have been testing out Mobian lately on my PinePhone Pro and decided to add deb files for some of my most used apps. To grab the debs, head to the git pages below and the links to the files are in the README file for each app.

**IdokRemote - A Kodi remote**

https://gitlab.com/The3DmaN/idokremote

**LAMB - A Bible based on the World English Bible**

https://gitlab.com/The3DmaN/lord-almightys-modern-bible

**HA-Connect - A Web App for connecting to a Home Assistant web interface**

https://gitlab.com/The3DmaN/ha-connect
