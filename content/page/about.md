---
title: About The3DmaN
subtitle: 
comments: false
---

## Welcome

Thanks for visiting my page. I have recently uploaded many of the projects I am working on. I hope you enjoy them and are able to get some use out of them. 

All my projects are opensource distributed under a GPL v3 license. I primarily design apps for Linux mobile (PinePhone), but since I develop them initially on Linux desktop, most of them are compatable on there as well.